package com.example.peterbryce.kryptoapp;

import android.util.Base64;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.HashMap;

/**
 * Created by peterbryce on 28.09.17.
 */

public class GetPrivateKey {
    String privateKeyString = "";
    TabActivity tabActivity = new TabActivity();
    SendData sendData = new SendData();
    PrivateKey privateKey = null;
    RSA_Algorithmus rsa_algorithmus = new RSA_Algorithmus();

    public PrivateKey getPrivateKeyDB(final String telNumber){
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {


                HashMap<String,String> hashMap = new HashMap<String, String>();
                JSONObject myJson = new JSONObject();


                try {
                    myJson.put("user_to",telNumber);
                    myJson.put("action","GetPrivateKey");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                hashMap.put("json", myJson.toString());

                try {
                    sendData.SendData(hashMap);

                    for (int i=0;i<sendData.getArrayList().size();i++){
                        privateKeyString = sendData.getArrayList().get(i);

                        byte[] decodedBytes = Base64.decode(privateKeyString,Base64.NO_WRAP);

                        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
                        privateKey = keyFactory.generatePrivate(new PKCS8EncodedKeySpec(decodedBytes));

                        Log.d("PrivateKey",privateKey.toString());
                    }

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (InvalidKeySpecException e) {
                    e.printStackTrace();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }


            }
        });

        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return privateKey;

    }
}
