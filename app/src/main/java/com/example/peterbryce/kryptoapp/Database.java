package com.example.peterbryce.kryptoapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

import junit.framework.Test;

import java.util.ArrayList;

/**
 * Created by peterbryce on 03.05.17.
 */

public class Database extends SQLiteOpenHelper implements BaseColumns {


    public Database(Context context){
        super(context,"TestDB.db",null,1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //sqLiteDatabase.execSQL("DROP TABLE IF EXISTS");
        String CREATE_CONTACTS_TABLE="CREATE TABLE KRYPTOTEXT (KEY_ID INTEGER PRIMARY KEY, CRYPTTEXT TEXT,CONTACT TEXT)";
        sqLiteDatabase.execSQL(CREATE_CONTACTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        onCreate(sqLiteDatabase);
    }

    public void DatenSpeichern(String Item,String Contact){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("CRYPTTEXT",Item);
        values.put("CONTACT",Contact);

        //Zeileeinfügen
        db.insert("KRYPTOTEXT",null,values);
        db.close();

    }

    public ArrayList<String> Ausgabe(){
        String name="";
        ArrayList<String>DatabaseList= new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor =  db.rawQuery("Select * from KRYPTOTEXT",null);// Daten die in den ? kommen
        //cursor.moveToNext();

        //Database ausgabe
            while(cursor.moveToNext()){
                name = cursor.getString(1);
                DatabaseList.add(name);

            }

        //Log.d("Ausgabe",name);
        cursor.close();
        return DatabaseList;
    }

    public void delete(String ItemDelete){
        SQLiteDatabase db = getWritableDatabase();
        String[] selection={ItemDelete};
        db.delete("KRYPTOTEXT","CRYPTTEXT like ?",selection);
    }
}
