package com.example.peterbryce.kryptoapp;
import android.app.Activity;
import android.content.Context;
import android.os.Debug;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by peterbryce on 06.04.17.
 */

public class SubstitutionFunction {
    private Activity activity;
    String [] lowLetter = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"," "};
    int tempJ;
    String kryptText="";
    List<String>liste;


    public SubstitutionFunction(Activity act){
        this.activity = act;
    }

    public List getAlphabet(){
        return liste;
    }


    public String getCrypt(String myMessage){
        kryptText="";

        liste = Arrays.asList("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"," ");
        Collections.shuffle(liste);

        char[] charText = myMessage.toCharArray();

        for (int i=0;i<charText.length;i++){
            for (int j=0;j<lowLetter.length;j++){
                if(Character.toString(charText[i]).equals(lowLetter[j])){
                    tempJ=j;
                    String temp=" "+tempJ;
                }
            }
            kryptText += liste.get(tempJ);
            Log.d("Zahl1",""+tempJ);

        }
        return kryptText;
    }

    public  String deCrypt(String cryptedMessage,List<String>Databaseliste){

        String decryptedText="";
        char[] charText = cryptedMessage.toCharArray();

        for(int k=0;k<Databaseliste.size();k++){
            if(Databaseliste.get(k).equals("")){
                Databaseliste.set(k," ");
            }
        }


        for(int i=0;i<charText.length;i++){
            for (int j=0;j<Databaseliste.size();j++){

                if(Character.toString(charText[i]).equals(Databaseliste.get(j).toString())){
                    decryptedText += lowLetter[j];
                }

            }

        }

        return decryptedText;
    }

    public String getChooseCrypt(String myMessage,List<String>Databaseliste){
        kryptText="";

        String AlphabetTest= ""+Databaseliste;

        char[] charText = myMessage.toCharArray();

        for(int k=0;k<Databaseliste.size();k++){
            if(Databaseliste.get(k).equals("")){
                Databaseliste.set(k," ");
            }
        }

        for (int i=0;i<charText.length;i++){
            for (int j=0;j<lowLetter.length;j++){
                if(Character.toString(charText[i]).equals(lowLetter[j])){
                    tempJ=j;
                    String temp=" "+tempJ;
                    Log.d("Zahl3",""+tempJ);
                    Log.d("Zahl4",""+lowLetter[tempJ]);
                    Log.d("Zahl5",""+Databaseliste.get(tempJ));



                }
            }
            Log.d("Array",Databaseliste.toString());

            kryptText += Databaseliste.get(tempJ);

        }
        return kryptText;
    }
}
