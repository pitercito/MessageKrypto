package com.example.peterbryce.kryptoapp;

import android.app.Activity;
import android.util.Log;

/**
 * Created by peterbryce on 20.02.17.
 */

public class CesarFunction {
    private Activity activity;
    private String textCrypt = "";
    private String textDecrypt = "";
    private char [] upperLetter= {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',' ','(',')'};
    private char [] lowLetter =  {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',';','.',','};

    public CesarFunction(Activity act){
        this.activity = act;
    }

    public String getCrypt(String myMessage,String verschiebung){

        int shift=Integer.parseInt(verschiebung);
        textCrypt="";


            char[] charText = myMessage.toCharArray();


            for(int j=0;j<charText.length;j++){


                for(int i=0;i<lowLetter.length;i++){

                    if(charText[j]==lowLetter[i] || charText[j]==upperLetter[i]){


                        int verschiebungIndex=(i+shift)%29;

                        if(charText[j]==lowLetter[i]){
                            textCrypt+=lowLetter[verschiebungIndex];
                        }else if(charText[j]==upperLetter[i]){
                            textCrypt+=upperLetter[verschiebungIndex];
                        }
                    }
                }
            }
            return textCrypt;
    }

    public String deCrypt(String cryptedMessage,int verschiebung){

        textDecrypt = "";
        char[] charCrypt = cryptedMessage.toCharArray();

        for(int j=0;j<charCrypt.length;j++){


            for(int i=0;i<lowLetter.length;i++){

                if(charCrypt[j]==lowLetter[i] || charCrypt[j]==upperLetter[i]){


                    int verschiebungIndex=(29+i-verschiebung);
                    Log.d("Test",""+verschiebungIndex);
                    if (verschiebungIndex < 0) {
                        while (verschiebungIndex < 0) {
                            verschiebungIndex += 29;
                        }
                    } else {
                        verschiebungIndex = verschiebungIndex%29;
                    }


                    if(charCrypt[j]==lowLetter[i]){
                        textDecrypt+=lowLetter[verschiebungIndex];
                    }else if(charCrypt[j]==upperLetter[i]){
                        textDecrypt+=upperLetter[verschiebungIndex];
                    }
                }
            }
        }

        return textDecrypt;
    }

}
