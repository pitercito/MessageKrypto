package com.example.peterbryce.kryptoapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Debug;
import android.provider.BaseColumns;
import android.provider.Settings;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by peterbryce on 05.05.17.
 */

public class DatabaseSubstitution extends SQLiteOpenHelper implements BaseColumns {


    public DatabaseSubstitution(Context context) {
        super(context, "Test.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_CONTACTS_TABLE="CREATE TABLE ALPHABET (KEY_ID INTEGER PRIMARY KEY,GENERATEDALPHABET TEXT)";
        sqLiteDatabase.execSQL(CREATE_CONTACTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        onCreate(sqLiteDatabase);
    }

    public void SaveAlphabet (List<String>liste){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("GENERATEDALPHABET",liste.toString());

        Log.d("wurde","safe");

        //Zeileeinfügen
        db.insert("ALPHABET",null,values);

        db.close();

    }

    public List<String> Ausgabe(){
        String name="";
        ArrayList<String>DatabaseList= new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor =  db.rawQuery("Select * from Alphabet",null);// Daten die in den ? kommen

        //Database ausgabe
        while(cursor.moveToNext()){
            name = cursor.getString(1);
            DatabaseList.add(name);

        }
        cursor.close();
        return DatabaseList;
    }

    public List<String> AlphabetID(){
        String name="";
        ArrayList<String>AlphabetList= new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor =  db.rawQuery("Select * from Alphabet",null);// Daten die in den ? kommen

        //Database ausgabe
        while(cursor.moveToNext()){
            name = cursor.getString(0);
            AlphabetList.add("Alphabet "+name);

        }
        cursor.close();
        return AlphabetList;
    }

    public List<String> ChooseAlphabet(int i){
        String name;
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor =  db.rawQuery("Select * from Alphabet",null);// Daten die in den ? kommen
        cursor.move(i);
        name = cursor.getString(1);
        cursor.close();


        String s1=name;
        String replace = s1.replace("[","");
        String replace1 = replace.replace("]","");
        String replace2 = replace1.replace(" ","");

        List<String> myList = new ArrayList<String>(Arrays.asList(replace2.split(",")));

        //Log.d("ListeDB",myList.toString());


        return myList;
    }

    public void DeleteAlphabet(List<String>SelectedAlphabet){
        SQLiteDatabase db = getWritableDatabase();

        for (int i=0;i<SelectedAlphabet.size();i++){
            String [] selection={SelectedAlphabet.get(i)};
            db.delete("Alphabet","KEY_ID like ?",selection);
        }
    }
}
