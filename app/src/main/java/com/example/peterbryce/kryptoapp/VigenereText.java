package com.example.peterbryce.kryptoapp;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class VigenereText extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vigenere_text);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Vigenere");
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.getNavigationIcon().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        TextView editText = (TextView)findViewById(R.id.textView4);
        editText.setText("Bei der Vigenère Chiffre wird das Alphabet A-Z und a-z um ein festes Wort verschoben, ähnlich der Cesar Chiffre wird die Position der Buchstaben im Alphabet als Wert genommen und der Text nun um diesen verschoben. Anders als bei der Caesar Chiffre wird hier jedoch ein Array ( das Wort) benutzt und so der Text Wertlänge für Wertlänge verschlüsselt.\n" +
                "\n" +
                "Formel:?\n" +
                "Beispiel: Satz=n=Hi du wie geht’s dir. N= 8,9,4,21,23,9,5,7,5,8,20,27,19,4,9,18\n" +
                "Verschlüsselungs Wort =k= Haus k=8,1,21,19\n" +
                "Verschlüsselter Text = C\n" +

                "Ergebnis: C=pjzmdj´zmios´ecj C=16,10,26,13,4,10,27,26,13,9,15,19,27,5,3,10\n" +
                "Die Vigenère Chiffre ist ein verschlüsselungs Verfahren und relativ schnell und leicht in seiner Anwendung für den Benutzer, da der Schlüssel auch nur ein Wort ist lässt sich auch dieser leicht und unauffällig über externe Wege übertragen.\n" +
                "Dieses ist die mittlere Stufe der Verschlüsselung und lässt sich durch \"Brute Force\" Verfahren ermitteln wenn genug Daten und Text mit derselben Verschlüsselung vorhanden ist.\n");

        editText.setMovementMethod(new ScrollingMovementMethod());

    }
}
