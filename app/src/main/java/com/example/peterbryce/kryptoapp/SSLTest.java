package com.example.peterbryce.kryptoapp;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Scanner;

import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

/**
 * Created by peterbryce on 21.02.17.
 */

public class SSLTest implements Runnable {


    public void SSLVerbindung() throws IOException{

        Socket server = new Socket("10.0.2.2",7000);
        Scanner scanner = new Scanner(server.getInputStream());
        System.out.println(scanner.hasNextLine());


        PrintWriter writer = new PrintWriter(server.getOutputStream(),true);
        writer.println("Hat geklappt");

        server.close();
    }

    @Override
    public void run() {
        try {
            SSLVerbindung();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


