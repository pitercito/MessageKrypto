package com.example.peterbryce.kryptoapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Base64;
import android.util.Log;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;

/**
 * Created by peterbryce on 01.06.17.
 */

public class UserExists extends SQLiteOpenHelper {

    byte[] name=null;


    public UserExists(Context context){
        super(context,"UserData.db",null,1);

    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_CONTACTS_TABLE="CREATE TABLE UserExists (KEY_ID INTEGER PRIMARY KEY, TELEPHONE_NUMBER TEXT, PASSWORD TEXT,private_Key TEXT,public_key TEXT)";
        sqLiteDatabase.execSQL(CREATE_CONTACTS_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        onCreate(sqLiteDatabase);
    }

    public boolean CheckIfUserExists(){
        boolean UserThere=false;
        String number="";
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor =  db.rawQuery("Select * from UserExists",null);

        //Database ausgabe
        while(cursor.moveToNext()){
            number = cursor.getString(0);
            if(number.equals("1")){
                UserThere=true;
            }
        }
        cursor.close();
        return UserThere;
    }

    public void insertUser(String number, String password, PrivateKey private_Key){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("TELEPHONE_NUMBER",number);
        values.put("PASSWORD",password);

        byte [] privateKeyBytes=private_Key.getEncoded();
        values.put("private_Key", privateKeyBytes);

        /*byte [] publicKeyBytes=publicKey.getEncoded();
        values.put("public_Key", publicKeyBytes);*/

        //Zeileeinfügen
        db.insert("UserExists",null,values);
        db.close();
    }

    public String Ausgabe(){
        String name="";
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor =  db.rawQuery("Select * from UserExists",null);// Daten die in den ? kommen
        //cursor.moveToNext();

        //Database ausgabe
        while(cursor.moveToNext()){
            name = cursor.getString(1);
        }

        //Log.d("Ausgabe",name);
        cursor.close();
        return name;
    }

    public PrivateKey PrivateKeyAusgabe() {
        PrivateKey privateKey = null;
        byte[] name = null;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("Select * from UserExists", null);// Daten die in den ? kommen
        //cursor.moveToNext();

        //Database ausgabe
        while (cursor.moveToNext()) {
            name = cursor.getBlob(3);
        }

        //Log.d("Ausgabe",name);
        cursor.close();

        try {

            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            privateKey = keyFactory.generatePrivate(new PKCS8EncodedKeySpec(name));

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }

        return privateKey;
    }

    public void UpdatePrivateKey(PrivateKey privateKey){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        Log.d("NewPrivateKey",privateKey.toString());

        byte [] privateKeyBytes=privateKey.getEncoded();
        contentValues.put("private_key",privateKeyBytes);
        db.update("UserExists",contentValues,"KEY_ID="+1,null);
        db.close();
    }


    public PublicKey PublicKeyAusgabe(){
        PublicKey publicKey=null;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor =  db.rawQuery("Select * from UserExists",null);// Daten die in den ? kommen

        //Database ausgabe

        while(cursor.moveToNext()){
            name = cursor.getBlob(4);
        }

        //Log.d("Ausgabe",name);
        cursor.close();

        try {
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            publicKey = keyFactory.generatePublic(new X509EncodedKeySpec(name));

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }


        return publicKey;
    }
}
