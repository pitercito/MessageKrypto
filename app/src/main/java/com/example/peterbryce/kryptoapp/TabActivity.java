package com.example.peterbryce.kryptoapp;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.res.TypedArrayUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

public class TabActivity extends AppCompatActivity {

    static String userName;
    static String contactNumber;
    static String newUserName;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("CryptChat");
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);


        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tab, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            if(getArguments().getInt(ARG_SECTION_NUMBER)==2){
                View rootView = inflater.inflate(R.layout.fragment_tab, container, false);
                final ListView listView = (ListView)rootView.findViewById(R.id.ListeChat);

                ArrayList<String>arrayList = new ArrayList<String>();
                final ArrayList<String>phoneNumerArray = new ArrayList<String>();

                Cursor phones = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null,null, null);

                while (phones.moveToNext())
                {
                    String name=phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));


                    arrayList.add(name+" "+phoneNumber);
                    phoneNumerArray.add(phoneNumber);
                    ArrayAdapter<String>adapter = new ArrayAdapter<String>(getActivity(),R.layout.kontakttelefon, R.id.messages,arrayList);

                    listView.setAdapter(adapter);

                }

                phones.close();


                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),R.layout.kontakttelefon, R.id.messages,arrayList);
                listView.setAdapter(adapter);


                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                        ChatDatabase chatDatabase = new ChatDatabase(getContext());
                        userName=listView.getItemAtPosition(i).toString();
                        contactNumber=phoneNumerArray.get(i);


                        int temp=0;

                        if (chatDatabase.Ausgabe().size()>0){

                            int[] array = new int[chatDatabase.Ausgabe().size()];

                            for (int j=0;j<chatDatabase.Ausgabe().size();j++){
                                if(chatDatabase.Ausgabe().get(j).equals(listView.getItemAtPosition(i).toString())){
                                    changeToChat(getContext());
                                    array[j]=0;
                                }else{
                                    array[j]=1;
                                }
                            }

                            for(int j=0;j<array.length;j++){
                                temp=temp+array[j];

                                if(temp==chatDatabase.Ausgabe().size()){
                                    chatDatabase.addToChat(listView.getItemAtPosition(i).toString());
                                    changeToChat(getContext());
                                }
                            }

                        }else if(chatDatabase.Ausgabe().size()==0){
                            chatDatabase.addToChat(listView.getItemAtPosition(i).toString());
                            changeToChat(getContext());
                        }


                    }
                });

                return rootView;


            }else if(getArguments().getInt(ARG_SECTION_NUMBER)==1){

                View rootView = inflater.inflate(R.layout.fragment_tab, container, false);
                final ListView listView = (ListView)rootView.findViewById(R.id.ListeChat);
                final ChatDatabase chatDatabase = new ChatDatabase(getContext());

                ArrayAdapter<String>adapter= new ArrayAdapter<String>(getActivity(),R.layout.kontakttelefon, R.id.messages,chatDatabase.Ausgabe());
                listView.setAdapter(adapter);
                final ArrayList<String>phoneNumerArray = new ArrayList<String>();


                Cursor phones = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null,null, null);

                while (phones.moveToNext()) {
                    String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    phoneNumerArray.add(phoneNumber);
                }


                listView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
                    @Override
                    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {

                        contextMenu.add(Menu.NONE,0,0,"Delete").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener(){
                            @Override
                            public boolean onMenuItemClick(MenuItem menuItem) {
                                AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo();
                                final int Itemindex=info.position;

                                String chat = (String) listView.getItemAtPosition(Itemindex);
                                chatDatabase.delete(chat);
                                final ArrayAdapter<String>adapter = new ArrayAdapter<String>(getActivity(),R.layout.kontakttelefon, R.id.messages,chatDatabase.Ausgabe());
                                listView.setAdapter(adapter);

                                return true;
                            }
                        });

                    }



                });


                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                        ChatDatabase chatDatabase = new ChatDatabase(getContext());
                        userName=listView.getItemAtPosition(i).toString();
                        newUserName= userName.replaceAll("[^0-9]","");


                        for (int j =0;j<phoneNumerArray.size();j++){
                            if(phoneNumerArray.get(j).toString().equals(newUserName)){
                                contactNumber=newUserName;
                            }
                        }

                        changeToChat(getContext());
                    }
                });




                return rootView;

            }else{
                View rootView = inflater.inflate(R.layout.fragment_blank, container, false);
                final ListView listView = (ListView)rootView.findViewById(R.id.KryptoListe);
                ArrayList<String>arrayList= new ArrayList<String>();
                arrayList.add("Cesar-Chiffre");
                arrayList.add("Substitution");
                arrayList.add("Vigenere");

                final ArrayAdapter<String>adapter= new ArrayAdapter<String>(getActivity(),R.layout.kontakttelefon, R.id.messages,arrayList);
                listView.setAdapter(adapter);


                listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){

                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        Log.d("Position",""+i);
                        if(i==0){
                            changeToChiffre(getContext());
                        }
                        if(i==1){
                            changeToSubstitution(getContext());
                        }
                        if(i==2){
                            changeToVigenere(getContext());
                        }
                    }
                });



                return rootView;
            }


        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "CHAT";
                case 1:
                    return "KONTAKTLISTE";
                case 2:
                    return "INFO";
            }
            return null;
        }
    }
    public static void changeToChat(Context ctx) {
        ctx.startActivity(new Intent(ctx, MainActivity.class));
    }

    public static void changeToChiffre(Context ctx) {
        ctx.startActivity(new Intent(ctx, CHiffre.class));
    }

    public static void changeToSubstitution(Context ctx) {
        ctx.startActivity(new Intent(ctx, substitution_text.class));
    }

    public static void changeToVigenere(Context ctx) {
        ctx.startActivity(new Intent(ctx, VigenereText.class));
    }
}
