package com.example.peterbryce.kryptoapp;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.TextView;

public class CHiffre extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chiffre);

        Toolbar toolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Cesar Chiffre");
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.getNavigationIcon().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



        TextView textView = (TextView)findViewById(R.id.Chiffre);
        textView.setText(
                "Bei der Cesar Chiffre wird das Alphabet A-Z und a-z um einen festen Wert verschoben , dabei ist n= Position des Buchstaben im Alphabet, k= der Wert um den Verschoben wird und c= das daraus resultierende Alphabet (Die Ergebnisse aus n+k werden mod26 für die anzahl der Buchstaben genommen).\n" +
                        "\n" + "Formel : (n+k)mod26=c\n" +
                        "\n" + "Beispiel: A=n=1...Z=n=26 Wort: Haus n=23,15,18,20 k=5\n" +
                        "\n" + "(n+k)mod26=c  (23+5=28 15+5=20 18+5=23 20+5=25)mod26 \n" +
                        "\n" + "Ergebnis: C= 3,20,23,25 Wort: Ctwy\n" +
                        "\n" + "Die Cesar Chiffre ist das einfachste verschlüsselungs Verfahren und ist daher schnell und leicht in seiner Anwendung für den Benutzer, da der Schlüssel auch nur ein Wert ist lässt sich auch dieser leicht und unauffällig über externe Wege übertragen.\n" +
                        "\n" + "Dieses ist die schwächste Stufe der verschlüsselung und lässt sich relativ leicht durch \"Brute Force\" Verfahren ermitteln.\n");

        textView.setMovementMethod(new ScrollingMovementMethod());

    }
}
