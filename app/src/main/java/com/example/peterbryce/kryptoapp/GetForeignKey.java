package com.example.peterbryce.kryptoapp;

import android.os.Handler;
import android.os.Message;
import android.renderscript.Element;
import android.util.Base64;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;

/**
 * Created by peterbryce on 08.08.17.
 */


public class GetForeignKey {

    String foreignKey = "";
    TabActivity tabActivity = new TabActivity();
    SendData sendData = new SendData();
    PublicKey publicKey = null;
    RSA_Algorithmus rsa_algorithmus = new RSA_Algorithmus();

    public String getForeignKeyDB(){
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {


                HashMap<String,String> hashMap = new HashMap<String, String>();
                JSONObject myJson = new JSONObject();


                try {
                    myJson.put("user_to",tabActivity.contactNumber);
                    myJson.put("action","GetForeignKey");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                hashMap.put("json", myJson.toString());

                try {
                    sendData.SendData(hashMap);

                    for (int i=0;i<sendData.getArrayList().size();i++){
                        foreignKey = sendData.getArrayList().get(i);

                        byte[] decodedBytes = Base64.decode(foreignKey,Base64.NO_WRAP);
                        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
                        publicKey = keyFactory.generatePublic(new X509EncodedKeySpec(decodedBytes));

                        Log.d("Fremdenschlüssel",publicKey.toString());
                    }

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (InvalidKeySpecException e) {
                    e.printStackTrace();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }


            }
        });

        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return foreignKey;

    }
}
