package com.example.peterbryce.kryptoapp;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

public class Login extends AppCompatActivity {

    String telNumber="";
    String password="";
    SendData sendData = new SendData();
    UserExists userExists = new UserExists(this);
    GetPrivateKey getPrivateKey = new GetPrivateKey();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        TelephonyManager telephonyManager = (TelephonyManager)getSystemService(TELEPHONY_SERVICE);
        telNumber = telephonyManager.getLine1Number();

        final EditText editText =(EditText)findViewById(R.id.editText2);
        editText.setText(telNumber);

        final EditText passwordField =(EditText)findViewById(R.id.editText5);

        Button button = (Button)findViewById(R.id.login);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                password = passwordField.getText().toString();
                telNumber = editText.getText().toString();


                Thread t = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        HashMap<String,String> hashMap = new HashMap<String, String>();
                        JSONObject myJson = new JSONObject();

                        try {
                            myJson.put("telefonNumber",telNumber);
                            myJson.put("secret",password);
                            myJson.put("action","login");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        hashMap.put("json", myJson.toString());

                        try {
                            sendData.SendData(hashMap);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                        if(sendData.getArrayList().size()==0){
                            Log.d("Leer","Leer");
                        }else{

                            for (int i=0;i<sendData.getArrayList().size();i++){
                                Log.d("match",""+sendData.getArrayList().get(i));

                                //Log.d("Test",getPrivateKey.getPrivateKeyDB(telNumber).toString());

                                userExists.insertUser(telNumber,password,getPrivateKey.getPrivateKeyDB(telNumber));
                                startActivity(new Intent(Login.this,TabActivity.class));


                            }



                        }



                    }
                });

                t.start();
                try {
                    t.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        });

    }

}
