package com.example.peterbryce.kryptoapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class substitution_text extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_substitution_text);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Substitution");
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.getNavigationIcon().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
        //getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        TextView editText = (TextView)findViewById(R.id.vigenereText);
        editText.setText("Bei der Substitution wird das Alphabet genommen und neu Angeordnet ( Java Shuffle z.B.), es kann aber auch ein eigenes Alphabet geschaffen werden. Wichtig hierbei ist dass die Anzahl der Zeichen = der Anzahl des Ziel Alphabets ist. Danach wird jedes Zeichen des Ursprungs Alphabets durch den nun Neuen Buchstaben an der Position ersetzt. Dabei ist das Ausgangs Alphabet N und das Ziel Alphabet C.\n" +
                "\n"+
                "Formel: Alphabet(A) <-> Alphabet(B)\n" +
                "Beispiel : N=Hi du wie geht’s dir.\n" +
                "Schuffel Alphabet = C\n" +
                "A B C D E F G H I J K L M N O P Q R S T U V W X Y Z\n" +
                "D P L I Z Y M A X V T U N Q K W E C R S B G F H J O\n" +
                "N auf C Abbilden. \n" +
                "Ergebnis: Ax ib fxz mzas´r ixc.\n" +
                "\n"+
                "Die Substitution ist ein recht sicheres verschlüsselungs Verfahren und im vergleich zu den Anderen beiden angebotenen Verfahren Komplexer in der Anwendung und Nutzung , da ein komplettes Alphabet erstellt und übertragen werden muss. Die Übertragung auf Externen weg ist dementsprechend Aufwändig.\n");

        editText.setMovementMethod(new ScrollingMovementMethod());

    }

}
