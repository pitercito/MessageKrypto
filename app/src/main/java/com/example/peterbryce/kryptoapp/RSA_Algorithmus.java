package com.example.peterbryce.kryptoapp;

import android.util.Base64;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;

/**
 * Created by peterbryce on 12.08.17.
 */

public class RSA_Algorithmus {

    PublicKey publicKey;
    PrivateKey privateKey;
    String decryptString;
    Cipher cipher = null;
    byte[] cipherText;



    public byte[] Encrypt(String message,PublicKey publicKey){

        try {
            //KeyGenerator();
            cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE,publicKey);
            cipherText = cipher.doFinal(message.getBytes("UTF-8"));

            Log.d("PublicKeyRSA",publicKey.toString());


        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


        return cipherText;
    }

    public String Decrypt(byte[] encryptedMessage,PrivateKey privateKey){

        try {

            Log.d("Message",encryptedMessage.toString());
            Log.d("PrivteKeyRSA",privateKey.toString());

            Cipher cipher = null;
            cipher = Cipher.getInstance("RSA");

            cipher.init(Cipher.DECRYPT_MODE,privateKey);
            decryptString = new String(cipher.doFinal(encryptedMessage),"UTF-8");

        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }


        return  decryptString;
    }


}
