package com.example.peterbryce.kryptoapp;

import android.util.Log;

/**
 * Created by peterbryce on 22.05.17.
 */

public class VigenereFunction {

    String [] lowLetter = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"," "};


    public String crypt(String word,String text){

        String cryptedWord="";
        String schluessel="";
        char[] charText = text.toCharArray();
        char[] charWord = word.toCharArray();

        if(charWord.length < charText.length){
            int WieoftpasstdasWortrein = charText.length / charWord.length;
            int WieoftpasstderBuchstabe = charText.length % charWord.length;

            for(int i=0;i< WieoftpasstdasWortrein;i++){
                schluessel+=word;

            }

            for(int j=0;j< WieoftpasstderBuchstabe;j++){
                    schluessel+=charWord[j];

            }

        }else if(charWord.length > charText.length){
            for (int x=0;x<charText.length;x++){
                schluessel+=charWord[x];
            }
        }else if(charWord.length==charText.length){
            schluessel=word;
        }

        return algorithmus(schluessel,text);
    }

    public String algorithmus(String schluessel,String klartext){
        char[] textArray = klartext.toCharArray();
        char[] keyArray = schluessel.toCharArray();
        int tempJ=0;
        int tempX=0;
        int cryptedNumber;
        String cryptedText="";

        for(int i=0;i<textArray.length;i++){
            for(int j=0;j<lowLetter.length;j++){
                if(lowLetter[j].equals(Character.toString(textArray[i]))){
                    tempJ=j;
                }
            }
            for (int x=0;x<lowLetter.length;x++){
                if(lowLetter[x].equals(Character.toString(keyArray[i]))){
                    tempX=x;
                }
            }

            cryptedNumber=(tempJ+tempX)%53;
            cryptedText+=lowLetter[cryptedNumber];

        }


        return cryptedText;
    }


    public String decrypt(String schluessel,String klartext){
        char[] textArray = klartext.toCharArray();
        char[] keyArray = schluessel.toCharArray();
        int tempJ=0;
        int tempX=0;
        int cryptedNumber;
        String cryptedText="";


        if (keyArray.length < textArray.length) {
            int WieoftpasstdasWortrein = textArray.length / keyArray.length;
            int WieoftpasstderBuchstabe = textArray.length % keyArray.length;

            for (int i = 0; i < WieoftpasstdasWortrein; i++) {
                schluessel += schluessel;

            }

            for (int j = 0; j < WieoftpasstderBuchstabe; j++) {
                schluessel += keyArray[j];

            }
        }

        keyArray = schluessel.toCharArray();

        for(int i=0;i<textArray.length;i++){
            for(int j=0;j<lowLetter.length;j++){
                if(lowLetter[j].equals(Character.toString(textArray[i]))){
                    tempJ=j;
                }
            }
            for (int x=0;x<lowLetter.length;x++){
                if(lowLetter[x].equals(Character.toString(keyArray[i]))){
                    tempX=x;
                }
            }

            if(tempJ>=tempX){
                cryptedNumber=(tempJ-tempX)%53;
                cryptedText+=lowLetter[cryptedNumber];
            }else if(tempJ<tempX){
                cryptedNumber=(tempJ-tempX)+53;
                cryptedText+=lowLetter[cryptedNumber];
            }

        }


        return cryptedText;
    }
}
