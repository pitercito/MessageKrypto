package com.example.peterbryce.kryptoapp;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by peterbryce on 26.06.17.
 */

public class SendData {

    ArrayList<String>arrayList = new ArrayList<String>();


    public void SendData(HashMap<String, String> params) throws UnsupportedEncodingException {
        try {

            URL url = new URL("http://abe478.pstud0.mt.haw-hamburg.de/test/chat.php");
            HttpURLConnection connection = (HttpURLConnection)url.openConnection() ;
            //URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("POST");

            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());

            StringBuilder requestresult = new StringBuilder();
            boolean first = true;
            for (Map.Entry<String, String> entry : params.entrySet()) {
                if (first)
                    first = false;
                else
                    requestresult.append("&");
                    requestresult.append(URLEncoder.encode(entry.getKey(), "ASCII"));
                    requestresult.append("=");
                    requestresult.append(URLEncoder.encode(entry.getValue(), "ASCII"));
            }

            writer.write(requestresult.toString());
            writer.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line = null;

            while((line = reader.readLine()) != null){

                sb.append(line + "\n");

                Log.d("Newtwork run ",line.toString());
                arrayList.add(line.toString());

            }

            reader.close();


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public ArrayList<String>getArrayList(){
        return arrayList;
    }
}
