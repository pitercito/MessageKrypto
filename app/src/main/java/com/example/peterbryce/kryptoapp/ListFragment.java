package com.example.peterbryce.kryptoapp;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import junit.framework.Test;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class ListFragment extends Fragment {


    public ListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_list, container, false);

        ListView listView = (ListView)rootView.findViewById(R.id.liste);
        ArrayList<String>arrayList=new ArrayList<String>();
        arrayList.add("Peter");
        arrayList.add("Jordi");

        ArrayAdapter<String>adapter = new ArrayAdapter<String>(getActivity(),R.layout.list_item, R.id.messages,arrayList);
        listView.setAdapter(adapter);

        return rootView;
    }

}
