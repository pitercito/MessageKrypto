package com.example.peterbryce.kryptoapp;

import android.telephony.TelephonyManager;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

/**
 * Created by peterbryce on 26.06.17.
 */

public class ChatToDB {

    public void SendChatData(String Kontakt,String telefonnumber,String message) throws UnsupportedEncodingException {
        try {

            String data = URLEncoder.encode("Kontakt", "UTF-8")
                    + "=" + URLEncoder.encode(Kontakt, "UTF-8");

            data += "&" + URLEncoder.encode("telefonnumber", "UTF-8") + "="
                    + URLEncoder.encode(telefonnumber, "UTF-8");

            data += "&" + URLEncoder.encode("message", "UTF-8") + "="
                    + URLEncoder.encode(message, "UTF-8");



            URL url = new URL("http://abe478.pstud0.mt.haw-hamburg.de/test/chat.php");
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(data);
            writer.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line = null;

            while((line = reader.readLine()) != null){
                sb.append(line + "\n");
                Log.d("Newtwork run ",line.toString());
            }

            reader.close();


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void GetMessages(String Kontakt){
        try {

            String data = URLEncoder.encode("Kontakt", "UTF-8")
                    + "=" + URLEncoder.encode(Kontakt, "UTF-8");



            URL url = new URL("http://abe478.pstud0.mt.haw-hamburg.de/test/GetMessages.php");
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(data);
            writer.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line = null;

            while((line = reader.readLine()) != null){
                sb.append(line + "\n");
                Log.d("Newtwork run ",line.toString());
            }

            reader.close();


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
