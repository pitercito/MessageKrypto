package com.example.peterbryce.kryptoapp;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.InvalidParameterException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.interfaces.DSAKeyPairGenerator;
import java.security.interfaces.DSAParams;
import java.security.interfaces.DSAPrivateKey;
import java.util.HashMap;

public class Register_activity extends AppCompatActivity {

    EditText telTextNumber;
    EditText password;
    EditText username;
    static String telNumber;
    SendData sendData = new SendData();
    PublicKey publicKey;
    PrivateKey privateKey;
    String encodedString;
    String privateString;

    UserExists userExists = new UserExists(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_activity);

        Button registerButton = (Button) findViewById(R.id.registerButton);
        Button loginButton = (Button) findViewById(R.id.loginButton);
        telTextNumber = (EditText) findViewById(R.id.telnumber);
        password = (EditText) findViewById(R.id.password);
        username = (EditText) findViewById(R.id.username);

        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        telNumber = telephonyManager.getLine1Number();

        telTextNumber.setText(telNumber);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*if (telTextNumber.getText().toString().equals("")){
                    telNumber=telTextNumber.getText().toString();
                }*/

                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        HashMap<String,String> hashMap = new HashMap<String, String>();
                        JSONObject myJson = new JSONObject();


                        try {
                            KeyPair newPair=null;
                            KeyPairGenerator keyPair = KeyPairGenerator.getInstance("RSA");
                            keyPair.initialize(1064); //4096 wird zu langsam
                            newPair=keyPair.generateKeyPair();
                            publicKey = newPair.getPublic();
                            privateKey = newPair.getPrivate();


                            byte [] test= publicKey.getEncoded();
                            byte [] privateByte= privateKey.getEncoded();

                            encodedString = Base64.encodeToString(test,Base64.NO_WRAP);
                            privateString = Base64.encodeToString(privateByte,Base64.NO_WRAP);


                        } catch (NoSuchAlgorithmException e) {
                            e.printStackTrace();
                        }

                        userExists.insertUser(telTextNumber.getText().toString(),password.getText().toString(),privateKey);



                        try {

                            telNumber=telTextNumber.getText().toString();
                            myJson.put("telefonNumber",telTextNumber.getText().toString());
                            myJson.put("secret",password.getText().toString());
                            myJson.put("name",username.getText().toString());
                            myJson.put("action","register");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        hashMap.put("json", myJson.toString());



                        try {
                            sendData.SendData(hashMap);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                        HashMap<String,String> hashMapKeys = new HashMap<String, String>();
                        JSONObject myJsonKeys = new JSONObject();

                        try {
                            myJsonKeys.put("telefonNumber",telTextNumber.getText().toString());
                            myJsonKeys.put("publickey",encodedString);
                            myJsonKeys.put("action","publicKey");

                            hashMapKeys.put("json", myJsonKeys.toString());
                            sendData.SendData(hashMapKeys);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                        HashMap<String,String> hashMapPrivateKeys = new HashMap<String, String>();
                        JSONObject myJsonPrivateKeys = new JSONObject();

                        try {
                            myJsonPrivateKeys.put("telefonNumber",telTextNumber.getText().toString());
                            Log.d("PrivateString",privateString);
                            myJsonPrivateKeys.put("privatekey",privateString);
                            myJsonPrivateKeys.put("action","privatekey");

                            hashMapPrivateKeys.put("json", myJsonPrivateKeys.toString());
                            sendData.SendData(hashMapPrivateKeys);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                    }
                }).start();




                ChangeScreen();

            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Login();
            }
        });

    }

    public void ChangeScreen(){
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(Register_activity.this,TabActivity.class));
            }
        },100L);
    }

    public void Login(){
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(Register_activity.this,Login.class));
            }
        },100L);
    }

    public PublicKey GetPublicKey(){
        return publicKey;
    }

}
