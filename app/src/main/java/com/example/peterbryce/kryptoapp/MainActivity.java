package com.example.peterbryce.kryptoapp;

import android.app.Activity;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Debug;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import junit.framework.Test;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.security.InvalidKeyException;
import java.security.InvalidParameterException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.interfaces.DSAKeyPairGenerator;
import java.security.interfaces.DSAParams;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


public class MainActivity extends AppCompatActivity implements PopupMenu.OnMenuItemClickListener {

    private String editText;
    protected String myMessage;
    private ArrayList<String>arrayList;
    private ArrayAdapter<String>adapter;
    private PopupWindow cryptOptions;
    private PopupWindow SubstitutionOptions;
    private PopupWindow cesardecrypt;
    private String verschiebung;
    CesarFunction cesarFunction = new CesarFunction(this);
    SubstitutionFunction substitutionFunction = new SubstitutionFunction(this);
    ListView listView;
    private int Itemindex;
    String CesarDecrytedText;
    String SubstitutionText;
    Database database = new Database(this);
    DatabaseSubstitution databaseSubstitution  = new DatabaseSubstitution(this);
    ListView chooseAlphabet;
    ArrayAdapter<String>adapterAlphabet;
    int ChooseCursor;
    List<String>IDChooser;
    CharSequence [] test;
    VigenereFunction vigenereFunction  = new VigenereFunction();
    private EditText editTexto;
    private Button cryptedButton;
    boolean isReadyTest;
    TabActivity tabActivity = new TabActivity();
    SendData sendData = new SendData();
    String telNumber="";
    ChatToDB chatToDB = new ChatToDB();
    ChatDatabase chatDatabase = new ChatDatabase(this);
    String cryptedMessage;
    Register_activity register_activity = new Register_activity();
    UserExists userExists = new UserExists(this);
    GetForeignKey getForeignKey = new GetForeignKey();
    RSA_Algorithmus rsa_algorithmus = new RSA_Algorithmus();
    PublicKey publicKeyFromDB;
    byte[]crypt;
    ArrayList<String>arrayDecryt= new ArrayList<>();
    PublicKey publicKey = null;
    PrivateKey privateKey = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        myToolbar.setTitle(tabActivity.userName);
        myToolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(myToolbar);


        //Array Initialisierung und Adapter kommunikation
        listView = (ListView) findViewById(R.id.listView);
        registerForContextMenu(listView);

        //Get stored Strings from DATABASE
        GetMessages();

        Log.d("Size",""+arrayDecryt.size());

        if(arrayDecryt.size()>0){

            //Decrypt with Privatkey
            for (int i=0;i<arrayDecryt.size();i++){

                byte [] decodeBytes = Base64.decode(arrayDecryt.get(i),Base64.NO_WRAP);
                String decoedString = rsa_algorithmus.Decrypt(decodeBytes,userExists.PrivateKeyAusgabe());
                arrayDecryt.set(i,decoedString);
            }

            arrayList=arrayDecryt;

        }else{
            arrayList = new ArrayList<>();
        }


        adapter = new ArrayAdapter<String>(this, R.layout.list_item, R.id.messages, arrayList);
        listView.setAdapter(adapter);


        //Liste für Alphabet
        chooseAlphabet = (ListView) findViewById(R.id.AlphabetList);

        cryptedButton = (Button) findViewById(R.id.button);
        cryptedButton.setEnabled(false);
        TextInputControll();



    }

    public PublicKey CreatePublicKey(){
        //PublicKey wird hier wieder konstruiert von der DB

        PublicKey publicKey=null;

        try {
            byte[] decodedBytes = Base64.decode(getForeignKey.getForeignKeyDB(),Base64.NO_WRAP);
            KeyFactory keyFactory = keyFactory = KeyFactory.getInstance("RSA");
            publicKey = keyFactory.generatePublic(new X509EncodedKeySpec(decodedBytes));

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }

        return publicKey;

    }


    public void preparePackage(final String text){
        new Thread(new Runnable() {
            @Override
            public void run() {
                HashMap<String,String> hashMap = new HashMap<String, String>();
                JSONObject myJson = new JSONObject();


                try {
                    myJson.put("user_from",userExists.Ausgabe());
                    myJson.put("user_to",tabActivity.contactNumber);
                    myJson.put("text",text);
                    myJson.put("action","send");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                hashMap.put("json", myJson.toString());

                try {
                    sendData.SendData(hashMap);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }


            }
        }).start();
    }

    public ArrayList<String> GetMessages(){
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                HashMap<String,String> hashMap = new HashMap<String, String>();
                JSONObject myJson = new JSONObject();


                try {
                    myJson.put("user_from",userExists.Ausgabe());
                    myJson.put("user_to",tabActivity.contactNumber);
                    myJson.put("action","get");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                hashMap.put("json", myJson.toString());

                try {

                    sendData.SendData(hashMap);

                    for (int i=0;i<sendData.getArrayList().size();i++){
                        arrayDecryt.add(sendData.getArrayList().get(i));
                    }

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }


            }
        });

        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return sendData.getArrayList();
    }


    public void DeleteDB(final String text){
        new Thread(new Runnable() {
            @Override
            public void run() {
                HashMap<String,String> hashMap = new HashMap<String, String>();
                JSONObject myJson = new JSONObject();


                try {
                    myJson.put("user_from",userExists.Ausgabe());
                    myJson.put("user_to",tabActivity.contactNumber);
                    myJson.put("action","delete");
                    myJson.put("text",text);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                hashMap.put("json", myJson.toString());

                try {

                    sendData.SendData(hashMap);

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }


            }
        }).start();
    }

    public void TextInputControll(){

        editTexto = (EditText)findViewById(R.id.editText);

        editTexto.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                InputGreaterNULL();

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                InputGreaterNULL();
            }
        });
    }

    public void InputGreaterNULL(){
        boolean isReady =editTexto.getText().toString().length()>0;
        cryptedButton.setEnabled(isReady);
    }

    //MenuBar

    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.navigation_menu, menu);
        return true;
    }

    public void newGenerateKeys(){
        try {
            KeyPair newPair=null;
            KeyPairGenerator keyPair = KeyPairGenerator.getInstance("RSA");
            keyPair.initialize(1064); //4096 wird zu langsam
            newPair=keyPair.generateKeyPair();
            publicKey = newPair.getPublic();
            privateKey = newPair.getPrivate();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.alphabet_delete:

                //Liste für Alphabet
                adapterAlphabet = new ArrayAdapter<String>(this,R.layout.choose_alphabet,R.id.AlphabetName,databaseSubstitution.AlphabetID());

                final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Delete Alphabet");

                ////////Delete Alphabet////////

                test= new CharSequence[databaseSubstitution.AlphabetID().size()];


                for (int i=0;i<databaseSubstitution.AlphabetID().size();i++){
                    test[i] = databaseSubstitution.AlphabetID().get(i);
                }

                IDChooser = new ArrayList<>();

                builder.setMultiChoiceItems(test,null,new DialogInterface.OnMultiChoiceClickListener(){

                    @Override
                    public void onClick(DialogInterface dialogInterface, int i, boolean b) {

                        if(b){
                            IDChooser.add(test[i].toString().replace("Alphabet ",""));
                        }
                    }
                })

                        .setPositiveButton("DELETE", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                for (int j=0;j<IDChooser.size();j++){
                                    databaseSubstitution.DeleteAlphabet(IDChooser);
                                    adapter.notifyDataSetChanged();
                                }
                            }
                        });

                AlertDialog dialog = builder.create();
                builder.show();

                return true;

            case R.id.Back:

                startActivity(new Intent(MainActivity.this,TabActivity.class));
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();

        menu.add(Menu.NONE,0,0,"Cesar");
        menu.add(Menu.NONE,1,1,"Substitution");
        menu.add(Menu.NONE,2,2,"Vigenere");
        menu.add(Menu.NONE,3,3,"Delete");


        inflater.inflate(R.menu.decryption, menu);


    }

   @Override
   public boolean onContextItemSelected(final MenuItem item) {

       AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
       Itemindex=info.position;



        if(item.getItemId()==0){

            final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setMessage("Give Number!!!").setTitle("CesarDecrypt");
            final EditText input = new EditText(this);
            input.setInputType(InputType.TYPE_CLASS_NUMBER);

            builder.setPositiveButton("decrypt",new DialogInterface.OnClickListener(){

                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if(input.getText().toString().equals("")){

                    }else{
                        String cryptedMessage = (String) listView.getItemAtPosition(Itemindex);
                        CesarDecrytedText=cesarFunction.deCrypt(cryptedMessage,Integer.parseInt(input.getText().toString()));

                        final  AlertDialog.Builder newBuilder = new AlertDialog.Builder(MainActivity.this);
                        newBuilder.setMessage(CesarDecrytedText);
                        AlertDialog dialog = newBuilder.create();
                        newBuilder.show();
                    }


                }
            });

            AlertDialog dialog = builder.create();
            builder.setView(input);
            builder.show();


        }else if(item.getItemId()==1) {



            adapterAlphabet = new ArrayAdapter<String>(this,R.layout.choose_alphabet,R.id.AlphabetName,databaseSubstitution.AlphabetID());

            final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle("SubstitutionDecrypt");
            builder.setSingleChoiceItems(adapterAlphabet, 1, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            ChooseCursor=i+1;
                        }
                    });

                    builder.setPositiveButton("decrypt", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //dialogInterface.dismiss();
                            String cryptedMessage = (String) listView.getItemAtPosition(Itemindex);


                            SubstitutionText = substitutionFunction.deCrypt(cryptedMessage,databaseSubstitution.ChooseAlphabet(ChooseCursor));

                            final AlertDialog.Builder newBuilder = new AlertDialog.Builder(MainActivity.this);
                            newBuilder.setMessage(SubstitutionText);
                            AlertDialog dialog = newBuilder.create();
                            newBuilder.show();

                        }
                    });

            AlertDialog dialog = builder.create();
            builder.show();
        }else if(item.getItemId()==3){

            //Delete
            cryptedMessage = (String) listView.getItemAtPosition(Itemindex);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {


                    //Hier wird die Nachricht wieder verschlüsselt und gelöscht
                    try {

                        byte[] decodedBytes = Base64.decode(getForeignKey.getForeignKeyDB(),Base64.NO_WRAP);
                        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
                        PublicKey publicKey = keyFactory.generatePublic(new X509EncodedKeySpec(decodedBytes));
                        crypt=rsa_algorithmus.Encrypt(cryptedMessage,publicKey);
                        DeleteDB(Base64.encodeToString(crypt,Base64.NO_WRAP));

                    } catch (InvalidKeySpecException e) {
                        e.printStackTrace();
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    }



                    adapter.notifyDataSetChanged();


                }
            });



        }else if(item.getItemId()==2){

            final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setMessage("Give Word!!!").setTitle("VigenereDecrypt");
            final EditText input = new EditText(this);


            builder.setPositiveButton("decrypt", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if(input.getText().toString().equals("")){

                    }else{
                        String cryptedMessage = (String) listView.getItemAtPosition(Itemindex);
                        String VigenereDecrypted=vigenereFunction.decrypt(input.getText().toString(),cryptedMessage);

                        final  AlertDialog.Builder newBuilder = new AlertDialog.Builder(MainActivity.this);
                        newBuilder.setMessage(VigenereDecrypted);
                        AlertDialog dialog = newBuilder.create();
                        newBuilder.show();
                    }

                }
            });

            builder.setView(input);
            AlertDialog dialog = builder.create();
            builder.show();

        }
        return super.onContextItemSelected(item);
    }

    //Text senden an TextView
    public void textSenden(View view) {


        //PopMenu
        PopupMenu popupMenu = new PopupMenu(this,view);
        popupMenu.setOnMenuItemClickListener(this);
        MenuInflater inflater = popupMenu.getMenuInflater();
        inflater.inflate(R.menu.cryp_select,popupMenu.getMenu());
        popupMenu.show();

    }

    //Choose Item
    @Override
    public boolean onMenuItemClick(MenuItem item) {
        if (item.getItemId()== R.id.Cesar){

            final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setMessage("Give Number!!!").setTitle("CesarCrypt");
            final EditText input = new EditText(this);
            input.setInputType(InputType.TYPE_CLASS_NUMBER);

            builder.setPositiveButton("crypt",new DialogInterface.OnClickListener(){

                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if(input.getText().toString().equals("")){

                    }else{
                        EditText editText = (EditText)findViewById(R.id.editText);
                        myMessage = editText.getText().toString();

                        verschiebung = input.getText().toString();

                        String temp =cesarFunction.getCrypt(myMessage,verschiebung);
                        arrayList.add(temp);

                        crypt=rsa_algorithmus.Encrypt(temp,CreatePublicKey());
                        preparePackage(Base64.encodeToString(crypt,Base64.NO_WRAP));

                        adapter.notifyDataSetChanged();

                        editTexto.setText("");


                    }
                }
            });

            AlertDialog dialog = builder.create();
            builder.setView(input);
            builder.show();

            return true;
        }else if(item.getItemId()== R.id.Substitution){
            LayoutInflater layoutInflater=(LayoutInflater)getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);
            final ViewGroup container = (ViewGroup) layoutInflater.inflate(R.layout.substitution_options,null);
             SubstitutionOptions= new PopupWindow(container,ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT,true);
            RelativeLayout relativeLayout = (RelativeLayout)findViewById(R.id.activity_main);
            SubstitutionOptions.showAtLocation(relativeLayout, Gravity.CENTER,0,0);


            Button generateOwn = (Button)container.findViewById(R.id.button4);
            generateOwn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    EditText editText = (EditText)findViewById(R.id.editText);
                    myMessage = editText.getText().toString();

                    String temp = substitutionFunction.getCrypt(myMessage);
                    //database.DatenSpeichern(temp,tabActivity.userName);
                    arrayList.add(temp);

                    crypt=rsa_algorithmus.Encrypt(temp,CreatePublicKey());
                    preparePackage(Base64.encodeToString(crypt,Base64.NO_WRAP));


                    databaseSubstitution.SaveAlphabet(substitutionFunction.getAlphabet());

                    adapter.notifyDataSetChanged();
                    editTexto.setText("");
                    SubstitutionOptions.dismiss();
                }
            });


            Button ChooseAlphabet = (Button)container.findViewById(R.id.button7);

            //Liste für Alphabet
            adapterAlphabet = new ArrayAdapter<String>(this,R.layout.choose_alphabet,R.id.AlphabetName,databaseSubstitution.AlphabetID());

            ChooseAlphabet.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle("Choose Alphabet");
                    builder.setSingleChoiceItems(adapterAlphabet, 1, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            ChooseCursor=i+1;
                        }
                    });

                    builder.setPositiveButton("decrypt", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            EditText editText = (EditText)findViewById(R.id.editText);
                            myMessage = editText.getText().toString();

                            String temp=substitutionFunction.getChooseCrypt(myMessage,databaseSubstitution.ChooseAlphabet(ChooseCursor));
                            //database.DatenSpeichern(temp,tabActivity.userName);
                            arrayList.add(temp);

                            crypt=rsa_algorithmus.Encrypt(temp,CreatePublicKey());
                            preparePackage(Base64.encodeToString(crypt,Base64.NO_WRAP));


                            adapter.notifyDataSetChanged();
                            SubstitutionOptions.dismiss();

                        }
                    });

                    AlertDialog dialog = builder.create();
                    builder.show();

                    SubstitutionOptions.dismiss();
                }
            });



            //POPUPWindow Close
            Button closeButton = (Button)container.findViewById(R.id.button6);
            closeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SubstitutionOptions.dismiss();
                }
            });
        }else if(item.getItemId()== R.id.Vigenere){

            final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle("Give me a word");

            final EditText input = new EditText(this);
            builder.setView(input);

            EditText editText = (EditText)findViewById(R.id.editText);
            myMessage = editText.getText().toString();


            builder.setPositiveButton("crypt",new DialogInterface.OnClickListener(){

                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if(input.getText().toString().equals("")){

                    }else{
                        String temp=vigenereFunction.crypt(input.getText().toString(),myMessage);
                        database.DatenSpeichern(temp,tabActivity.userName);
                        arrayList.add(temp);

                        crypt=rsa_algorithmus.Encrypt(temp,CreatePublicKey());
                        preparePackage(Base64.encodeToString(crypt,Base64.NO_WRAP));

                        adapter.notifyDataSetChanged();
                        editTexto.setText("");
                    }

                }
            });


            AlertDialog dialog = builder.create();
            builder.show();

        }else if(item.getItemId()== R.id.Normal){

            EditText editText = (EditText)findViewById(R.id.editText);
            myMessage = editText.getText().toString();
            database.DatenSpeichern(myMessage,tabActivity.userName);
            arrayList.add(myMessage);

            crypt=rsa_algorithmus.Encrypt(myMessage,CreatePublicKey());
            preparePackage(Base64.encodeToString(crypt,Base64.NO_WRAP));

            adapter.notifyDataSetChanged();
            editTexto.setText("");

            newGenerateKeys();
            userExists.UpdatePrivateKey(privateKey);
            Log.d("NewPrivateCompare",userExists.PrivateKeyAusgabe().toString());
        }

        return false;
    }
}
