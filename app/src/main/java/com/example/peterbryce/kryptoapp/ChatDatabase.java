package com.example.peterbryce.kryptoapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import java.util.ArrayList;

/**
 * Created by peterbryce on 07.06.17.
 */

public class ChatDatabase extends SQLiteOpenHelper implements BaseColumns {

    public ChatDatabase(Context context){
        super(context,"ChatDB.db",null,1);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_CONTACTS_TABLE="CREATE TABLE CHAT (KEY_ID INTEGER PRIMARY KEY, CHATVERLAUF TEXT)";
        sqLiteDatabase.execSQL(CREATE_CONTACTS_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        onCreate(sqLiteDatabase);
    }

    public void addToChat(String amigo){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("CHATVERLAUF",amigo);

        //Zeileeinfügen
        db.insert("CHAT",null,values);
        db.close();
    }

    public ArrayList<String> Ausgabe(){
        String name="";
        ArrayList<String>DatabaseList= new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor =  db.rawQuery("Select * from CHAT",null);// Daten die in den ? kommen
        //cursor.moveToNext();

        //Database ausgabe
        while(cursor.moveToNext()){
            name = cursor.getString(1);
            DatabaseList.add(name);

        }

        //Log.d("Ausgabe",name);
        cursor.close();
        return DatabaseList;
    }

    public void delete(String ItemDelete){
        SQLiteDatabase db = getWritableDatabase();
        String[] selection={ItemDelete};
        db.delete("CHAT","CHATVERLAUF like ?",selection);
    }

}
